\section{Background and Related Works}
\label{sec:back}

\subsection{Write cache for RAID}
\label{sec:write_cache}

Figure~\ref{fig:write_cache} illustrates a RAID controller that employs a write cache. 
The write cache employs non-volatile memory or a battery-back memory~\cite{wow, stow} in order to protect data loss in case of power-failure on the system.
\cb{A read cache is not assumed in this paper to isolate the performance gain of the write cache from the read cache, as did in previous works~\cite{wow, stow}.}
%As stated in previous works~\cite{wow, stow}, this setting maximizes read requests, which could interfere with write-backs, resulting in the most adversarial setting for destage algorithms~\cite{wow, stow}.}

\begin{figure}
\centering
\includegraphics[width=0.40\textwidth]{./fig/odg/write_cache.eps}
\caption{\cb{A typical RAID controller architecture employing a write cache. The data strcuture of the write cache is adopted from WOW~\cite{wow}, which is a state-of-the-art write cache design for RAID of HDDs.}}
\label{fig:write_cache}
\vspace{-\baselineskip}
\end{figure}

As long as there is free space in the write cache, an incoming write request is stored in the write cache and the write request is immediately committed to the requester.
\cb{Otherwise, the request should be pending until the write cache becomes available.}
The stored request is moved to the actual disks later in background.
This process is called \textit{destaging} and the write operations to the disk issued for destaging are called \textit{destage writes}.
A destage write is split into multiple \textit{strips}.
Depending on the RAID configuration, a parity strip may be added.
Each strip is stored in its corresponding disk.
The strips and the parity strip issued for a destage write are called \textit{stripe}.
The write requests belonging to a same stripe is called \textit{write group}.

To fully exploit benefits of a write cache, a cache controller should be designed to leverage temporal and spatial locality as well as to resist bursty writes~\cite{wow, stow}.
Temporal locality is leveraged by keeping hot data as much as possible.
On the other hand, to resist bursty writes, any stored data in the cache should be destaged as soon as possible so that there is enough room in the cache for the incoming requests.
Since these two objectives are often conflict, the cache controller should consider their trade-offs.
In the context of HDDs, leveraging spatial locality means minimizing mechanical movement inside a HDD.

The cache controller should make two fundamental decisions: when to destage and what to destage.

There are two basic approaches to decide when to destage.
One approach is to destage in idle time~\cite{the_architecture_of_a}.
It destages dirty blocks as quickly as possible~\cite{stow}.
This approach gives higher priority on the resistance to bursty writes than the temporal locality.
On the other hand, the linear threshold scheduling~\cite{destage_algorithms_for_disk} balances those two objectives.
When the occupancy of the write cache is low, it destages slowly to exploit the locality.
Its destaging rate increases proportionally to the occupancy to avoid lack of free space.

What to destage is usually related with the spatial locality within an individual HDD.
Extensive research has been conducted on the scheduling algorithms to exploit the spatial locality in a HDD~\cite{haining:2000}.
For RAID controllers, WOW~\cite{wow} has been proposed that exploits both spatial locality and temporal locality at the same time.
One step further, STOW~\cite{stow} enhances WOW by separating random writes from sequential writes.

\cb{However, to the best of our knowledge, there has been no prior discussion on the write cache design for \textit{an array of SSDs}.
GC is one of important characteristics of SSD that must be considered because GC takes long time and incoming requests cannot be serviced while GC is running.}

In the domain of SSDs, GC has attracted researchers' interest because GC has significant impact on the overall performance of SSDs~\cite{gftl, rftl, Han:2006:IGC, jlee:ispass11, ykim:msst11}.
As for the array of SSDs, Harmonia~\cite{ykim:msst11} has been proposed to reduce the impact of GC by coordinating GCs of SSDs.

%\cred{Write motivation about why write cache design needs to consider the characteristics of drives? What's wrong if they are not taken into account? Why SSDs are different from HDDs, then, you could use the paragraphs below for explaining this.}

%Flash memory-based storage devices need an erase operation~\cite{Niijima95:IBMJournal} before over-writing new data over old data.
%A write operation can be performed only to an erased page where an erase operation was performed before.
%Since the erase operation is significantly slower than read or write operations, out-of-place writes are usually employed.
%The out-of-place writes require garbage collection (GC) that cleans stale data to provide erased pages ready to be written.

%GC is an important characteristic of an SSD that should be considered to design the write cache for an array of SSDs.
%Page read usually takes 0.025 ms, page write takes 0.2 ms, and block erase takes 1.5 ms~\cite{msr_ssd}.
%GC takes at least 1.5 ms because GC requires at least one erase operation.
%In our experiments, we have empirically observed that GC takes 3 ms on average.
%While GC is running, any incoming requests and destage writes cannot be serviced, which means that they could be delayed until GC completes.
%Considering 3 ms is much larger than 0.025 ms and 0.2 ms, we can expect that GC has significant impact on the overall performance.

\cb{GC incurs significant variation in access time of SSD.
HDD also exhibits variation in access time, which mainly caused by the variation in physical location of requested data.
The variation is usually addressed by scheduling algorithms that leverage spatial locality.
In contrast, since GC is not related with location, existing scheduling algorithms cannot address the variation caused by GC.
Therefore, a new mechanism is required to mitigate the impact of GC in the array of SSDs.}

\subsection{State-of-The-Art Write Cache Design}

We first study the state-of-the-art write cache design for disk array, and 
in next Section, we present our proposed algorithms for SSD-aware write cache algorithm. 
Surprisingly not many works has studied write cache algorithms, however, 
Gill et. al.~\cite{wow, stow} revisited write buffering algorithms for the array of hard drives, 
and explored spatial and temporal localities in worklaods to improve the performance 
of the write cache algorithms~\cite{wow, stow}. 
Even if WOW~\cite{wow}, and STOW~\cite{stow} significantly improve write perforemance, however, 
their work are limited to the array of hard drives. 
On the contrary, our work, more importantly, focuses on the design of write cache for 
the array of SSDs that consider the internal characteristics of SSDs with more diverse resources than 
HDDs such as multiple chips, channels, multiple cores, etc. 

\begin{figure}
\centering
\includegraphics[width=0.30\textwidth]{./fig/odg/wow.eps}
\caption{The baseline data structure that leverages both temporal and spatial locality, which is adopted from WOW~\cite{wow}. Throughout this paper, this data structure is called \textit{write queue}.}
\label{fig:wow}
\vspace{-\baselineskip}
\end{figure}

%\cred{Junghee, can you improve your write-ups below using Fig1?}
\cb{A write hit can be either a hit on a \textit{strip} or a hit on a \textit{write group}.
Recall that a write group consists of multiple strips, as illustrated in Figure~\ref{fig:write_cache}.}
A hit on a strip means there already exists the requested strip in the write cache.
\cb{Even if the requested strip does not exist, its write group could exist (hit on a write group).
Since a parity strip is computed for each write group and parity is computed when a write group is destaged, a hit on a write group could reduce the number of parity computation.}
%Upon write miss, a new write group is allocated if there is no corresponding write group (not a hit on a stripe).
%If a hit on a stripe occurs, a new strip is allocated and the strip is stored in the existing, corresponding write group.
\cb{Write groups are always ordered by their logical block address (LBA), which leverages spatial locality.}

When a destaging is necessary, the destage pointer advances.
It will be discussed soon to determine when to destage.
If the recency bit of the write group pointed by the destage pointer is zero, the write group is destaged.
If the recency bit is one, the write group is retained and the recency bit is cleared.
Then the destage pointer advances again until it finds a write group whose recency bit is zero.
The recency bit is set when the write group is hit.
The recency bit gives one more chance for hit write groups to survive for one more cycle, which leverages temporal locality.

WOW adopts a linear threshold scheduling~\cite{destage_algorithms_for_disk} to determine when to destage.
In said scheme, two thresholds are involved: low and high thresholds.
When the number of write groups stored in the write cache is below the low threshold, destaging is not triggered.
When the number exceeds the low threshold, destaging gets started.
The destaging rate increases proportionally to the number of write groups until it reaches the high threshold.
Exceeding the high threshold, the write groups are destaged at a full rate.

\cb{As mentioned before, WOW does not address the variation caused by GC.
Since GC has significant impact on the overall system performance, there must be a mechanism to mitigate its impact.
In the following section, a novel mechanism is presented that addresses the variation caused by GC.}
