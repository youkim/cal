#set size 0.8, 1.4
set size 1.0, 1.0

set terminal postscript eps enhanced "" 35
set boxwidth 0.9 relative
set style fill solid 1.0
set style data histogram
#set style histogram clustered gap 1
#set style fill solid border -1
set xtics ("RAID0" 0, "RAID5" 1)
#set xtics rotate by -30
#set key outside top horizontal
#set key outside right reverse Left
set key horizontal

#set xlabel "Configuration" offset 0,-1
#set xlabel "Configuration" 
set ylabel "Average response time (ms)" offset 1
#set ylabel "Normalized Response Time w.r.t WOW" 
#offset 1

#set lmargin 4
set yrange[0:9]
set xrange[-0.5:1.6]

set output "conf.eps"
plot 'conf.dat' using 2 ti 'None' fill pattern 9,\
'' using 3 ti 'ISC' fill pattern 0,\
'' using 4 ti 'WOW' fill pattern 3,\
'' using 5 ti 'SIW' fill pattern 4
