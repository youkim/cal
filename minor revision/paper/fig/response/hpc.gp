#set size 0.8, 1.5
set size 1.0, 1.0

set terminal postscript eps enhanced "" 35
set boxwidth 0.9 relative
set style fill solid 1.0
set style data histogram
#set style histogram clustered gap 1
#set style fill solid border -1
set xtics ("HPC-G-0" 0, "HPC-G-5" 1, "Exchange-G-0" 2, "Exchange-G-5" 3, "Exchange-P-0" 4, "Exchange-P-5" 5)
set xtics rotate by -30
#set key outside top right horizontal reverse Left
#set key outside right reverse Left

#set xlabel "Configuration" offset 0,-1
#set xlabel "Configuration" 
#set ylabel "Average response time" offset 1
#set ylabel "Normalized Response Time w.r.t WOW" 
set ylabel "Normalized Response Time"
#offset 1

#set lmargin 4
set yrange[0:6]

#set output "hpc.eps"
#plot 'hpc.dat' using 2 ti 'WOW' fill pattern 3,\
#'' using 4 ti 'SIW' fill pattern 4

set yrange[0:1.35]
#set xrange[-0.5:1.7]
set output "hpc_norm.eps"
plot \
'hpc.dat' using ($2/$2) ti 'WOW' fill pattern 3,\
 '' using ($4/$2) ti 'SIW' fill pattern 4
