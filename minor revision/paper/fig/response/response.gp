set size 1.5, 1.5
#set size 1.5, 2.0

set terminal postscript eps enhanced "" 45
set boxwidth 0.9 relative

set style fill solid 1.0
set style data histogram
set style histogram clustered gap 1
set style fill solid border -1
#set xtics ("W_{small}" 0, "W_{medium}" 1, "W_{base}" 2, "W_{large}" 3, "W_{high}" 4, "W_{low}" 5, "W_{read}" 6, "W_{write}" 7)
set xtics ("W(4,20,60)" 0, "W(512,20,60)" 1, "W(1024,20,60)" 2, "W(4096,20,60)" 3, "W(1024,10,60)" 4, "W(1024,40,60)" 5, "W(1024,20,20)" 6, "W(1024,20,80)" 7)
#set xtics rotate by -90
set xtic rotate by -30 scale  0
#set key outside top horizontal reverse Left
#set key outside top horizontal
#set key outside right

#set xlabel "Workloads"
#set ylabel "Average response time" offset 1
#set ylabel "Normalized response time w.r.t WOW" offset 1
set ylabel "Normalized response time" offset 1

#set lmargin 4
#set yrange[0:1.2]

set output "raid0.eps"
plot 'raid0.dat' using 2 ti 'WOW' fill pattern 3,\
 '' using 4 ti 'SIW' fill pattern 4

set output "raid5.eps"
plot 'raid5.dat' using 2 ti 'WOW' fill pattern 3,\
 '' using 4 ti 'SIW' fill pattern 4


set yrange[0.4:1.25]

set output "raid0_norm.eps"
plot 'raid0.dat' using ($2/$2) ti 'WOW' fill pattern 3,\
 '' using ($4/$2) ti 'SIW' fill pattern 4

set yrange[0.4:1.25]
#set label "2.47" offset -1.8, 19.2
#set label "1.82" offset 2.6, 19.2
set label "2.47" offset -1.8, 16.2
set label "1.82" offset 2.6, 16.2

set output "raid5_norm.eps"
plot 'raid5.dat' using ($2/$2) ti 'WOW' fill pattern 3,\
 '' using ($4/$2) ti 'SIW' fill pattern 4
