set size 1.8, 1.8

set terminal postscript eps enhanced "Arial" 50
set boxwidth 0.9 relative
set style fill solid 1.0
set style fill solid border -1

set xlabel "Response time (msec)"
set ylabel "Cumulative distribution function" offset 1

set lmargin 4
#set yrange[0:140]
set xrange[0:140]

set output "cdf.eps"
set multiplot
plot \
'cdf_wow' using 1:4 every 10 ti 'WOW' with lp lw 3 ps 3 pt 6,\
'cdf_align' using 1:4 every 10 ti 'SIW' with lp lw 3 ps 3

set origin 0.5, 0.4
set size 1.2, 1.0
set xrange[0:140]
set yrange[0.98:1]
set xlabel ''
set ylabel ''

plot \
'cdf_wow' using 1:4 every 10 ti '' with lp lw 3 ps 3 pt 6,\
'cdf_align' using 1:4 every 10 ti '' with lp lw 3 ps 3

set nomultiplot
