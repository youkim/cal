#set size 1.8, 1.8
#set size 0.7, 1.5
#set size 1.0, 0.8
set size 1.0, 1.0

#set terminal postscript eps enhanced "Arial" 50
#set terminal postscript eps enhanced "Arial" 40
set terminal postscript eps enhanced "" 35
set boxwidth 0.9 relative
set style fill solid 1.0
set style fill solid border -1

set xlabel "Number of synchronized GC"
#set xlabel "Sync'ed GC (#)"
set ylabel "Probability" offset 2
#set xtics (">=8" 1, ">=7" 2, ">=6" 3, ">=5" 4, ">=4" 5, ">=3" 6, ">=2" 7, ">=1" 8)
set xtics (">7" 1, ">6" 2, ">5" 3, ">4" 4, ">3" 5, ">2" 6, ">1" 7, ">0" 8)
#set xtics format ">=%x"
set ytics 0.2

set lmargin 5
#set yrange[0:140]
#set xrange[0:140]
#set key left reverse Left
set key top center

set output "gc_raid0.eps"
plot \
'gc_wow_raid0' using 1:5 ti 'WOW' with lp lw 3 ps 3 pt 6,\
'gc_align_raid0' using 1:5 ti 'SIW' with lp lw 3 ps 3
